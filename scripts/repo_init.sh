#!/bin/bash

# init Repo
git submodule update --init --recursive


#build gtest
PWD_=$(pwd)
cd googletest        # Main directory of the cloned repository.
mkdir build          # Create a directory to hold the build output.
cd build
cmake ..             # Generate native build scripts for GoogleTest.

make
sudo make install    # Install in /usr/local/ by default

cd ${PWD_}

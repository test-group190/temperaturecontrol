#!/bin/bash

PWD_=$(pwd)
cd ${PWD_}/test/

cmake .
make clean
make
./runTests

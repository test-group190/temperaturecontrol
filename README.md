# TemperatureControl

This is a simple temperature control system that controls a heater and a cooler to keep the temperature within a desired range.
There are two class implementation, TemperatureController class and TemperatureObserver class.The TemperatureController class is responsible for keeping track of the current temperature and notifying its Observers when the temperature changes, while the TemperatureObserver class is responsible for controlling the heating and cooling systems based on the current temperature.

# Design Pattern
## Observer Pattern:

The Observer pattern is a behavioral pattern that defines a one-to-many dependency between objects so that when one object changes state, all its dependents are notified and updated automatically. In this case, the temperature sensor can act as the subject and the air conditioning units can act as observers. When the temperature changes, the sensor will notify all the air conditioning units, which will adjust their settings accordingly to maintain the desired temperature. Using the Observer pattern allows for decoupling of the subject (temperature sensor) and observers (air conditioning units), which makes the system more flexible and maintainable. It also allows for easy addition of new observers and removal of existing ones without affecting the behavior of the subject.

# Requirements

The following software libraries are required:

* CMakeLists
* gtest

# Project Setup
Inorder to start with the project please follow these steps initially:
* Clone the repository
* Open project directory : cd temperaturecontrol

Run the repo_init.sh file from the project directory inorder to install all the predependencies and include the google test as a submodule.

```bash
./scripts/repo_init.sh
```

# Build Instructions
To build the system, follow these steps:

* cd temperaturecontrol

Run the buildScript.sh inside the scripts/ folder in order to build and run the project. This uses CMake to build.

```bash
./scripts/buildScript.sh
```

# UnitTesting
To run the tests, follow these steps:

* cd temperaturecontrol

Run the run_unittest.sh inside the scripts/ folder in order to run the unittest for the project.

```bash
./scripts/run_unittest.sh
```


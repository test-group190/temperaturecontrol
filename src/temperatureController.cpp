/* Includes ------------------------------------------------------------------*/
#include <iostream>
#include <algorithm>
#include "temperatureController.h"

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* External variables --------------------------------------------------------*/

/* Public user code ----------------------------------------------------------*/

// Subject class that notifies Observers of temperature changes
TemperatureController::TemperatureController(float minTemp, float maxTemp)
    : m_minTemp(minTemp), m_maxTemp(maxTemp), m_currentTemp(0)
{
    // Initialize the current temperature to the midpoint of the range
    m_currentTemp = (minTemp + maxTemp) / 2.0f;
}

void TemperatureController::addObserver(Observer *observer)
{
    m_observers.push_back(observer);
}

void TemperatureController::removeObserver(Observer *observer)
{
    m_observers.erase(std::remove(m_observers.begin(), m_observers.end(), observer), m_observers.end());
}

void TemperatureController::updateTemperature(float newTemp)
{
    m_currentTemp = newTemp;
    if (m_currentTemp < m_minTemp)
    {
        // Temperature is too low, notify observers to start heating
        notifyObservers(true, false);
    }
    else if (m_currentTemp > m_maxTemp)
    {
        // Temperature is too high, notify observers to start cooling
        notifyObservers(false, true);
    }
    else
    {
        // Temperature is within the acceptable range, notify observers to stop heating and cooling
        notifyObservers(false, false);
    }
}

float TemperatureController::getMinTemp()
{
    return m_minTemp;
}

float TemperatureController::getMaxTemp()
{
    return m_maxTemp;
}

void TemperatureController::notifyObservers(bool startHeating, bool startCooling)
{
    for (Observer *observer : m_observers)
    {
        observer->update(m_currentTemp);
    }
}

/* Includes ------------------------------------------------------------------*/
#include "temperatureObserver.h"

#include <iostream>
/* Private define ------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Public user code ----------------------------------------------------------*/

TemperatureObserver::TemperatureObserver(TemperatureController *controller)
    : m_controller(controller), m_isHeating(false), m_isCooling(false)
{
    m_controller->addObserver(this);
}

TemperatureObserver::~TemperatureObserver()
{
    m_controller->removeObserver(this);
}

void TemperatureObserver::update(float temperature)
{
    if (temperature < m_controller->getMinTemp() && !m_isHeating)
    {
        // Temperature is too low, start heating
        startHeating();
    }
    else if (temperature > m_controller->getMaxTemp() && !m_isCooling)
    {
        // Temperature is too high, start cooling
        startCooling();
    }
    else if ((temperature >= m_controller->getMinTemp() && m_isHeating) ||
             (temperature <= m_controller->getMaxTemp() && m_isCooling))
    {
        // Temperature is within acceptable range, stop heating and cooling
        stopHeating();
        stopCooling();
    }
}

void TemperatureObserver::startHeating()
{
    m_isHeating = true;
    std::cout << "Starting heating..." << std::endl;
    // API call to start the heating system
}

void TemperatureObserver::startCooling()
{
    m_isCooling = true;
    std::cout << "Starting cooling..." << std::endl;
    // API call to start the cooling system
}

void TemperatureObserver::stopHeating()
{
    m_isHeating = false;
    std::cout << "Stopping heating..." << std::endl;
    // API call to stop the heating system
}

void TemperatureObserver::stopCooling()
{
    m_isCooling = false;
    std::cout << "Stopping cooling..." << std::endl;
    // API call to stop the cooling system
}

//getters
bool TemperatureObserver::isHeating()
{
    return m_isHeating;
}

bool TemperatureObserver::isCooling()
{
    return m_isCooling;
}
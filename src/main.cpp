
/* Includes ------------------------------------------------------------------*/
#include <iostream>
#include "temperatureController.h"
#include "temperatureObserver.h"

/* 
This implementation separates the concerns of the TemperatureController and TemperatureObserver classes. 
The TemperatureController class is responsible for keeping track of the current temperature 
and notifying its Observers when the temperature changes, while the TemperatureObserver 
class is responsible for controlling the heating and cooling systems based on the current temperature.

Observer Pattern is Implemented for the project:
The Observer pattern allows for more flexibility and modularity in the design of the program, 
as it decouples the subject and observer objects and allows for the addition of multiple observers 
that can respond to changes in the subject.

*/

/* Public user code ----------------------------------------------------------*/

int main() {
    // Set the acceptable temperature range
    float minTemp = 18.0f;
    float maxTemp = 24.0f;

    // Create a TemperatureController object on the heap
    TemperatureController* controller = new TemperatureController(minTemp, maxTemp);

    // Create a TemperatureObserver object on the heap to control the heating and cooling systems
    TemperatureObserver* observer = new TemperatureObserver(controller);

    // Simulate a change in temperature
    float newTemp = 45.5f;
    controller->updateTemperature(newTemp);

    // Clean up allocated memory
    delete observer;
    delete controller;

    return 0;
}
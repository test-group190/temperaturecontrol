#ifndef OBSERVERINTERFACE_H
#define OBSERVERINTERFACE_H

/* Includes ------------------------------------------------------------------*/

/* Public typedef -----------------------------------------------------------*/

/* Public macro -------------------------------------------------------------*/

/* Public function prototypes -----------------------------------------------*/

// Abstract Observer class
class Observer {
public:
    virtual void update(float temperature) = 0;
};

#endif // OBSERVERINTERFACE_H
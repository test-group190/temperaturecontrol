#ifndef TEMPERATURECONTROLLER_H
#define TEMPERATURECONTROLLER_H

/* Includes ------------------------------------------------------------------*/
#include <vector>
#include "observerInterface.h"

/* Public typedef -----------------------------------------------------------*/

/* Public macro -------------------------------------------------------------*/

/* Public function prototypes -----------------------------------------------*/

// Subject class that notifies Observers of temperature changes
class TemperatureController {
public:
    TemperatureController(float minTemp, float maxTemp);

    void addObserver(Observer* observer);

    void removeObserver(Observer* observer);

    void updateTemperature(float newTemp);
    float getMinTemp();
    float getMaxTemp();
    
private:
    float m_minTemp;
    float m_maxTemp;
    float m_currentTemp;
    std::vector<Observer*> m_observers;

    void notifyObservers(bool startHeating, bool startCooling);
};

#endif // TEMPERATURECONTROLLER_H
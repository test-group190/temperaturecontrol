#ifndef TEMPERATUREOBSERVER_H
#define TEMPERATUREOBSERVER_H

/* Includes ------------------------------------------------------------------*/
#include "temperatureController.h"

/* Public typedef -----------------------------------------------------------*/

/* Public macro -------------------------------------------------------------*/

/* Public function prototypes -----------------------------------------------*/


class TemperatureObserver : public Observer {
public:
    TemperatureObserver(TemperatureController* controller);
    ~TemperatureObserver();

    void update(float temperature) override;
    bool isHeating();
    bool isCooling();

private:
    TemperatureController* m_controller;
    bool m_isHeating;
    bool m_isCooling;

    void startHeating();
    void startCooling();
    void stopHeating();
    void stopCooling();

};

#endif // TEMPERATUREOBSERVER_H


//============================================================================
// Name        : run_tests.cpp
// Description : main file to kick the C/C++ tests
//============================================================================

#include "gtest/gtest.h"

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
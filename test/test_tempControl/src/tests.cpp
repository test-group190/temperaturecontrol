#include "gtest/gtest.h"
#include "temperatureController.h"
#include "temperatureObserver.h"


TEST(TemperatureObserverTest, TemperatureInRange) {
    // Set the acceptable temperature range
    float minTemp = 18.0f;
    float maxTemp = 24.0f;

    // Create a TemperatureController object
    TemperatureController controller(minTemp, maxTemp);

    // Create a TemperatureObserver object to control the heating and cooling systems
    TemperatureObserver observer(&controller);

    // Simulate a change in temperature within the acceptable range
    float newTemp = 22.0f;
    controller.updateTemperature(newTemp);

    // Check that heating and cooling were not started
    EXPECT_FALSE(observer.isHeating());
    EXPECT_FALSE(observer.isCooling());
}

TEST(TemperatureObserverTest, TemperatureTooLow) {
    // Set the acceptable temperature range
    float minTemp = 18.0f;
    float maxTemp = 24.0f;

    // Create a TemperatureController object
    TemperatureController controller(minTemp, maxTemp);

    // Create a TemperatureObserver object to control the heating and cooling systems
    TemperatureObserver observer(&controller);

    // Simulate a change in temperature that is too low
    float newTemp = 15.0f;
    controller.updateTemperature(newTemp);

    // Check that heating was started and cooling was not started
    EXPECT_TRUE(observer.isHeating());
    EXPECT_FALSE(observer.isCooling());
}

TEST(TemperatureObserverTest, TemperatureTooHigh) {
    // Set the acceptable temperature range
    float minTemp = 18.0f;
    float maxTemp = 24.0f;

    // Create a TemperatureController object
    TemperatureController controller(minTemp, maxTemp);

    // Create a TemperatureObserver object to control the heating and cooling systems
    TemperatureObserver observer(&controller);

    // Simulate a change in temperature that is too high
    float newTemp = 27.0f;
    controller.updateTemperature(newTemp);

    // Check that cooling was started and heating was not started
    EXPECT_FALSE(observer.isHeating());
    EXPECT_TRUE(observer.isCooling());
}